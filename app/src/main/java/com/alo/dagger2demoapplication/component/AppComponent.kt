package com.alo.dagger2demoapplication.component

import android.content.Context
import com.alo.dagger2demoapplication.App
import com.alo.dagger2demoapplication.MainActivity
import com.alo.dagger2demoapplication.Main2Activity
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component
interface AppComponent {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): AppComponent
    }

    // ---------- application
    fun inject(app: App)


    fun inject(mainActivity: MainActivity)
    fun inject(main2Activity: Main2Activity)
}
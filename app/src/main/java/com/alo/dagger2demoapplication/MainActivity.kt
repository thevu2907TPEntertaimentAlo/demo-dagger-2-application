package com.alo.dagger2demoapplication

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.alo.dagger2demoapplication.component.AppComponent

class MainActivity : BaseActivity() {
    override fun inject(component: AppComponent) {
        component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sharedPreferencesManager.save("TEST", "TEST SAVE DATA LOCAL CONFIG WITH DAGGER 2 INJECT")
        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(Intent(this, Main2Activity::class.java))
        }, 3000)
    }
}
package com.alo.dagger2demoapplication

import android.os.Bundle
import android.widget.Toast
import com.alo.dagger2demoapplication.component.AppComponent

class Main2Activity : BaseActivity() {
    override fun inject(component: AppComponent) {
        component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        val textSave = sharedPreferencesManager.getString("TEST")
        Toast.makeText(this, "status: $textSave", Toast.LENGTH_LONG).show()
    }
}
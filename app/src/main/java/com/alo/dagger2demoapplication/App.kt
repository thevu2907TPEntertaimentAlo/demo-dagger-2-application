package com.alo.dagger2demoapplication

import android.app.Application
import com.alo.dagger2demoapplication.component.AppComponent
import com.alo.dagger2demoapplication.component.DaggerAppComponent

class App: Application() {
    val appComponent: AppComponent by lazy {
        DaggerAppComponent.factory().create(applicationContext)
    }

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
    }
}
package com.alo.dagger2demoapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.alo.dagger2demoapplication.component.AppComponent
import javax.inject.Inject

abstract class BaseActivity: AppCompatActivity() {
    @Inject
    lateinit var sharedPreferencesManager: SharedPreferencesManager

    abstract fun inject(component: AppComponent)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject((application as App).appComponent)
    }
}
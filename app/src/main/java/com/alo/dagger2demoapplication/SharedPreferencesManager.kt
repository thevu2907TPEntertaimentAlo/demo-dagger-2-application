package com.alo.dagger2demoapplication

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPreferencesManager @Inject constructor(context: Context) {
    private val defaultValueLong: Long = -1
    private val defaultValueInteger = -1
    private val defaultValueFloat = -1
    private val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    fun save(key: String?, value: Boolean) {
        prefs.edit().putBoolean(key, value).apply()
    }

    fun save(key: String?, value: String?) {
        prefs.edit().putString(key, value).apply()
    }

    fun save(key: String?, value: Float) {
        prefs.edit().putFloat(key, value).apply()
    }

    fun save(key: String?, value: Int) {
        prefs.edit().putInt(key, value).apply()
    }

    fun save(key: String?, value: Long) {
        prefs.edit().putLong(key, value).apply()
    }

    fun getBoolean(key: String?): Boolean {
        return prefs.getBoolean(key, false)
    }

    fun getString(key: String?): String? {
        return prefs.getString(key, null)
    }

    fun getLong(key: String?): Long {
        return prefs.getLong(
            key, defaultValueLong
        )
    }

    fun getInt(key: String?): Int {
        return prefs.getInt(
            key, defaultValueInteger
        )
    }

    fun getFloat(key: String?): Float {
        return prefs.getFloat(
            key, defaultValueFloat.toFloat()
        )
    }

    fun remove(key: String?) {
        prefs.edit().remove(key).apply()
    }
}